# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 08:12:47 2022

@author: Vladimir Brauner
"""

import os

import networkx as nx
import numpy as np
import scipy as sp
from numpy import linalg as LA
import pandas as pd
import csv
import matplotlib.pylab as plt

import networkx as nx
from packaging import version
import sys 
import math
import random

from collections import Counter

print("Python version:", sys.version)
print("networkx version:", nx.__version__)

# assert networkx version is greater or equal to 2.6
assert version.parse(nx.__version__) >= version.parse("2.6")

# assert python version is greater that 3.7
assert sys.version_info[0] == 3
assert sys.version_info[1] >= 7 

# If working in colab mount the drive filesystem 
if 'google.colab' in str(get_ipython()):
    print('Working in colab')
    
    from google.colab import drive
    drive.mount('/content/drive')
else:
    print("working locally")
    
    
from abc import ABC
from abc import abstractmethod
import networkx as nx
import numpy as np


G1 = nx.read_graphml("../fb100/fb100/Caltech36.graphml", node_type=int)
G2 = nx.read_graphml("../fb100/fb100/USFCA72.graphml", node_type=int)
G3 = nx.read_graphml("../fb100/fb100/Haverford76.graphml", node_type=int)
G4 = nx.read_graphml("../fb100/fb100/Rice31.graphml", node_type=int)
G5 = nx.read_graphml("../fb100/fb100/Reed98.graphml", node_type=int)
G6 = nx.read_graphml("../fb100/fb100/Simmons81.graphml", node_type=int)
G7 = nx.read_graphml("../fb100/fb100/Oberlin44.graphml", node_type=int)
G8 = nx.read_graphml("../fb100/fb100/Colgate88.graphml", node_type=int)
G9 = nx.read_graphml("../fb100/fb100/Trinity100.graphml", node_type=int)
G10 = nx.read_graphml("../fb100/fb100/Wesleyan43.graphml", node_type=int)


GTest = nx.read_graphml("../fb100/fb100/Texas84.graphml", node_type=int)


class LinkPrediction(ABC):
    
    def __init__(self, graph):
        """
        Constructor
        
        Parameters
        ----------
        graph : Networkx graph
        """
        
        self.graph = graph
        self.N = len(graph)
        

    def neighbors(self,v):
        
        
        """
        Return the neighbors list of a node
        
        Parameters
        ----------
        v: int
        node id
        
        Return
        ------
        neighbors_list : python list
        
        """
        
        neighbors_list = self.graph.neighbors(v)
        return list(neighbors_list)

    #@abstractmethod
    #def fit (self):
    #    raise NotImplementedError("Fit must be implemented ")

class CommonNeighbors(LinkPrediction):
    def __init__(self ,graph):
            super(CommonNeighbors , self).__init__(graph)
    
    def link_prediction(self,v,w):
        
        vNeighbors = set(self.neighbors(v))
        wNeighbors = set(self.neighbors(w))
        common = list(vNeighbors & wNeighbors)
        
        return(len(common))
            
            
            
class Jaccard(LinkPrediction):
    def __init__(self , graph):
            super(Jaccard , self).__init__( graph )
            
    def link_prediction(self, v, w):
        
        vNeighbors = set(self.neighbors(v))
        wNeighbors = set(self.neighbors(w))
        inter = list(vNeighbors & wNeighbors)
        
        num = len(inter)
        denom = len(list(vNeighbors)) + len(list(wNeighbors)) - num
        
        if(denom == 0):
            return(0)
        
        return(num/denom)
        
        
    
class AdamicAdar(LinkPrediction):
    def __init__(self , graph):
            super(AdamicAdar , self).__init__(graph)
    
    def link_prediction(self, v, w):
        
        vNeighbors = set(self.neighbors(v))
        wNeighbors = set(self.neighbors(w))
        inter = list(vNeighbors & wNeighbors)
        S = 0
        for i in range(0, len(inter)):
            toZ = self.neighbors(inter[i])
            
            S = S + (1/math.log(len(toZ),10))
        
        return(S)


def evaluate_link_predictor(G,f,n):
    
    nbOfEdges = G.number_of_edges()
    frac = math.ceil(f*nbOfEdges)
    Eremoved = []
    print("deleting edges")
    print(frac)
    
    for i in range (0, frac):
        
        k = random.randint(0,G.number_of_edges() - 1)
        currentEdges = list(G.edges)
        edge = currentEdges[k]
        Eremoved.append(edge)
        G.remove_edge(edge[0], edge[1])
    
        
    
    print("edges deleted")
    print("computing link predictors")
    
    nbOfNodes = G.number_of_nodes()
    
    EpredictCommon = []
    EpredictJaccard = []
    EpredictAdamic = []
    
    Common = CommonNeighbors(G)
    Jaccardd = Jaccard(G)
    Adamic = AdamicAdar(G)
    
    for j in range(0, nbOfNodes):
        for l in range(0, nbOfNodes):
            
            if(j == l):
                continue
            
            common = Common.link_prediction(j,l)
            jaccard = Jaccardd.link_prediction(j,l)
            adamic = Adamic.link_prediction(j,l)
            
            EpredictCommon.append((j,l,common))
            EpredictJaccard.append((j,l,jaccard))
            EpredictAdamic.append((j,l,adamic))
            
    print("sorting list")
    EpredictCommon.sort(key = lambda x: x[2], reverse=True)

    EpredictJaccard.sort(key = lambda x: x[2], reverse=True)
    EpredictAdamic.sort(key = lambda x: x[2], reverse=True)
    
    print("truncating lists")
    
    EnC = []
    EnJ = []
    EnA = []
    
    for m in range(0,n):
        
        EnC.append((EpredictCommon[m][0], EpredictCommon[m][1]))
        EnJ.append((EpredictJaccard[m][0],EpredictJaccard[m][1]))
        EnA.append((EpredictAdamic[m][0],EpredictAdamic[m][1]))
    
    print("compute intersection size")
    
    linkPredictorCommon = len(list(set(EnC) & set(Eremoved)))
    linkPredictorJaccard = len(list(set(EnJ) & set(Eremoved)))
    linkPredictorAdamic = len(list(set(EnA) & set(Eremoved)))
    
    print(linkPredictorCommon)
    print(linkPredictorJaccard)
    print(linkPredictorAdamic)
    
    

evaluate_link_predictor(GTest,0.05,10000)
evaluate_link_predictor(GTest,0.1,10000)
evaluate_link_predictor(GTest,0.15,10000)
evaluate_link_predictor(GTest,0.2,10000)

    























    
