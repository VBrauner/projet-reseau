# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 17:06:13 2022

@author: Vladimir Brauner
"""

#initialisation 

import networkx as nx
import numpy as np
import scipy as sp
from numpy import linalg as LA
import pandas as pd
import csv
import matplotlib.pylab as plt

import networkx as nx
from packaging import version
import sys 

print("Python version:", sys.version)
print("networkx version:", nx.__version__)

# assert networkx version is greater or equal to 2.6
assert version.parse(nx.__version__) >= version.parse("2.6")

# assert python version is greater that 3.7
assert sys.version_info[0] == 3
assert sys.version_info[1] >= 7 

# If working in colab mount the drive filesystem 
if 'google.colab' in str(get_ipython()):
    print('Working in colab')
    
    from google.colab import drive
    drive.mount('/content/drive')
else:
    print("working locally")


def scraper_graph_caltech():
    
    G = nx.read_graphml("../fb100/fb100/Caltech36.graphml", node_type=int)
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    
    for i in range (0,numberOfNodes):
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figA.jpg", dpi=300)
    


def scraper_graph_caltech_log():
    
    G = nx.read_graphml("../fb100/fb100/Caltech36.graphml", node_type=int)
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    
    for i in range (0,numberOfNodes):
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    plt.xscale("log")
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figAlog.jpg", dpi=300)

def scraper_graph_mit():
    
    G = nx.read_graphml("../fb100/fb100/MIT8.graphml", node_type=int)
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    
    for i in range (0,numberOfNodes):
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figB.jpg", dpi=300)
    


def scraper_graph_mit_log():
    
    G = nx.read_graphml("../fb100/fb100/MIT8.graphml", node_type=int)
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    
    for i in range (0,numberOfNodes):
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    plt.xscale("log")
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figBlog.jpg", dpi=300)


def scraper_graph_jh():
    
    G = nx.read_graphml("../fb100/fb100/Johns Hopkins55.graphml", node_type=int)
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    print(numberOfNodes)
    for i in range (0,numberOfNodes):
        print(i)
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figC.jpg", dpi=300)
    


def scraper_graph_jh_log():
    
    G = nx.read_graphml("../fb100/fb100/Johns Hopkins55.graphml", node_type=int)
    
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    
    for i in range (0,numberOfNodes):
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    plt.xscale("log")
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figClog.jpg", dpi=300)
    
def scraper_graph_mit():
    
    G = nx.read_graphml("../fb100/fb100/MIT8.graphml", node_type=int)
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    print(numberOfNodes)
    for i in range (0,numberOfNodes):
        print(i)
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figB.jpg", dpi=300)
    


def scraper_graph_mit_log():
    
    G = nx.read_graphml("../fb100/fb100/MIT8.graphml", node_type=int)
    numberOfNodes = G.number_of_nodes()
    abscisse = []
    ordonnee = []
    
    for i in range (0,numberOfNodes):
        abscisse.append(G.degree[i])
        ordonnee.append(nx.clustering(G,i))
        
    
    plt.figure()
    plt.xscale("log")
    
    plt.plot(abscisse, ordonnee, "+")
    
    plt.savefig("figBlog.jpg", dpi=300)
    

print("Caltech graph")
scraper_graph_caltech()
print("caltech graph log")
scraper_graph_caltech_log()
print("mit graph")
scraper_graph_mit()
print("mit graph log")
scraper_graph_mit_log()
print("jh graph")
scraper_graph_jh()
print('jh graph log')
scraper_graph_jh_log()






