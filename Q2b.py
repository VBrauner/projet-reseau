# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 16:19:00 2022

@author: Vladimir Brauner
"""


#initialisation 

import networkx as nx
import numpy as np
import scipy as sp
from numpy import linalg as LA
import pandas as pd
import csv
import matplotlib.pylab as plt

import networkx as nx
from packaging import version
import sys 

print("Python version:", sys.version)
print("networkx version:", nx.__version__)

# assert networkx version is greater or equal to 2.6
assert version.parse(nx.__version__) >= version.parse("2.6")

# assert python version is greater that 3.7
assert sys.version_info[0] == 3
assert sys.version_info[1] >= 7 

# If working in colab mount the drive filesystem 
if 'google.colab' in str(get_ipython()):
    print('Working in colab')
    
    from google.colab import drive
    drive.mount('/content/drive')
else:
    print("working locally")




# load graphs (networkx)

GCaltech = nx.read_graphml("../fb100/fb100/Caltech36.graphml", node_type=int)
GMit = nx.read_graphml("../fb100/fb100/MIT8.graphml", node_type=int)
GJohnsHopkins = nx.read_graphml("../fb100/fb100/Johns Hopkins55.graphml", node_type=int)

def Adjency(G):
    
     numberOfNodes = G.number_of_nodes()
     nodeOrder = []
     
     for i in range(0,numberOfNodes):
         nodeOrder.append(i)
    
     A = np.array(nx.adjacency_matrix(G, nodelist=nodeOrder).todense())
     
     return(A)
         


def global_cluster(G):
    A = Adjency(G)
    ACube = np.linalg.matrix_power(A,3)
    triangle = (1/6) * np.trace(ACube)
    
    ASquare = np.linalg.matrix_power(A, 2)
    
    SumASquare = ASquare.sum()
    TrASquare = np.trace(ASquare)
    
    triple = (1/2) * (SumASquare - TrASquare)
    return 3 * (triangle / triple)



def avg_local_cluster(G):
    
    return(nx.average_clustering(G))

           

def graph_density(G):
    
    return(nx.density(G))

print(graph_density(GJohnsHopkins))
#print(graph_density(GMit))
print(graph_density(GCaltech))

print(global_cluster(GMit))
print(global_cluster(GJohnsHopkins))


