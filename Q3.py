# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 09:29:46 2022

@author: Vladimir Brauner
"""
import os
import seaborn as sns

import networkx as nx
import numpy as np
import scipy as sp
from numpy import linalg as LA
import pandas as pd
import csv
import matplotlib.pylab as plt

import networkx as nx
from packaging import version
import sys 

from collections import Counter

print("Python version:", sys.version)
print("networkx version:", nx.__version__)

# assert networkx version is greater or equal to 2.6
assert version.parse(nx.__version__) >= version.parse("2.6")

# assert python version is greater that 3.7
assert sys.version_info[0] == 3
assert sys.version_info[1] >= 7 

# If working in colab mount the drive filesystem 
if 'google.colab' in str(get_ipython()):
    print('Working in colab')
    
    from google.colab import drive
    drive.mount('/content/drive')
else:
    print("working locally")
    
    
    
def assort_fac():
    
    path = '../fb100/fb100'
 
    files = os.listdir(path)
    ab=[]
    ordo = []
    i = 0
    for name in files:
        print('assort_fac')
        print(i)
        print(name)
        i = i+1
        G = nx.read_graphml("../fb100/fb100/"+name, node_type=int)
        ab.append(G.number_of_nodes())
        ordo.append(nx.numeric_assortativity_coefficient(G, 'student_fac'))

    plt.figure()
    plt.xscale("log")
    plt.plot(ab, ordo, "o")
    plt.axhline(y=0, linestyle='--')
    
    plt.savefig("fig1.jpg", dpi=300)
    
    
def assort_major():
    
    path = '../fb100/fb100'
 
    files = os.listdir(path)
    ab=[]
    ordo = []
    i = 0
    for name in files:
        print('assort_major')
        print(i)
        print(name)
        i = i+1
        G = nx.read_graphml("../fb100/fb100/"+name, node_type=int)
        ab.append(G.number_of_nodes())
        ordo.append(nx.numeric_assortativity_coefficient(G, 'major_index'))

    plt.figure()
    plt.xscale("log")
    plt.plot(ab, ordo, "o")
    plt.axhline(y=0, linestyle='--')
    
    plt.savefig("fig2.jpg", dpi=300)
    
    
def assort_dorm():
    
    path = '../fb100/fb100'
 
    files = os.listdir(path)
    ab=[]
    ordo = []
    
    i = 0
    for name in files:
        print('assort_dorm')
        print(i)
        print(name)
        i = i+1
        G = nx.read_graphml("../fb100/fb100/"+name, node_type=int)
        ab.append(G.number_of_nodes())
        ordo.append(nx.numeric_assortativity_coefficient(G, 'dorm'))

    plt.figure()
    plt.xscale("log")
    plt.plot(ab, ordo, "o")
    plt.axhline(y=0, linestyle='--')
    
    plt.savefig("fig3.jpg", dpi=300)
    
    
    
#########################################################
#Les distributions

def distribution_fac():
   
    coef = []
    path = '../fb100/fb100'
 
    files = os.listdir(path)
    
    i = 0
    for name in files:
        print('distribution_fac')
        print(i)
        print(name)
        i = i+1
        G = nx.read_graphml("../fb100/fb100/"+name, node_type=int)
        coef.append(nx.numeric_assortativity_coefficient(G, 'student_fac'))
    
    
    ax = sns.distplot(coef)
    plt.savefig("fig4.jpg", dpi=300)
    
 

def distribution_major():
   
    coef = []
    path = '../fb100/fb100'
 
    files = os.listdir(path)
    
    i = 0
    for name in files:
        print('distribution_major')
        print(i)
        print(name)
        i = i+1
        G = nx.read_graphml("../fb100/fb100/"+name, node_type=int)
        coef.append(nx.attribute_assortativity_coefficient(G, 'major_index'))
        
    ax = sns.distplot(coef)
    plt.savefig("fig5.jpg", dpi=300)

def distribution_dorm():
   
    coef = []
    path = '../fb100/fb100'
 
    files = os.listdir(path)
    i = 0
    for name in files:
        print('distribution_dorm')
        print(i)
        print(name)
        i = i+1
        G = nx.read_graphml("../fb100/fb100/"+name, node_type=int)
        coef.append(round(nx.attribute_assortativity_coefficient(G, 'dorm'), 3))
        
    ax = sns.distplot(coef)
    plt.savefig("fig6.jpg", dpi=300)

assort_fac()
assort_major()
assort_dorm()

distribution_fac()
distribution_major()
distribution_dorm()




