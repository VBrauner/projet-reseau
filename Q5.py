# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 11:49:15 2022

@author: Vladimir Brauner
"""
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 14 14:35:26 2022

@author: VBrauner
"""


#initialisation 

import networkx as nx
import numpy as np
import scipy as sp
from numpy import linalg as LA
import pandas as pd
import csv
import matplotlib.pylab as plt

from packaging import version
import sys 

print("Python version:", sys.version)
print("networkx version:", nx.__version__)

# assert networkx version is greater or equal to 2.6
assert version.parse(nx.__version__) >= version.parse("2.6")

# assert python version is greater that 3.7
assert sys.version_info[0] == 3
assert sys.version_info[1] >= 7 

# If working in colab mount the drive filesystem 
if 'google.colab' in str(get_ipython()):
    print('Working in colab')
    
    from google.colab import drive
    drive.mount('/content/drive')
else:
    print("working locally")


import sknetwork as skn
from sklearn.metrics.cluster import normalized_mutual_info_score
    
from networkx.drawing.layout import spring_layout
import random
from collections import Counter
from collections import Counter
from random import choice
import copy
import math

G1 = nx.read_graphml("../fb100/fb100/Columbia2.graphml", node_type=int)
G2 = nx.read_graphml("../fb100/fb100/USFCA72.graphml", node_type=int)
G3 = nx.read_graphml("../fb100/fb100/Haverford76.graphml", node_type=int)
G4 = nx.read_graphml("../fb100/fb100/Rice31.graphml", node_type=int)
G5 = nx.read_graphml("../fb100/fb100/Reed98.graphml", node_type=int)
G6 = nx.read_graphml("../fb100/fb100/Yale4.graphml", node_type=int)
G7 = nx.read_graphml("../fb100/fb100/Brown11.graphml", node_type=int)
G8 = nx.read_graphml("../fb100/fb100/Cal65.graphml", node_type=int)
G9 = nx.read_graphml("../fb100/fb100/Baylor93.graphml", node_type=int)
G10 = nx.read_graphml("../fb100/fb100/Harvard1.graphml", node_type=int)

def propag_label_test(G,f,attribute_name):
    
    
    nbOfNodes = G.number_of_nodes()
    frac = math.ceil(f*nbOfNodes)
    L = []
    Lter = []
    LBis = []
    
    for p in range(nbOfNodes):
        LBis.append(G.nodes[p][attribute_name])
    
    LBis.sort()
    lengthbis = len(LBis)
    
    print("removing attributes and saving initial attribute values")
   
    for i in range (0,frac):
        
        k = random.randint(0,nbOfNodes-1)
        
        while k in Lter:
            k = random.randint(0,nbOfNodes-1)
        L.append((k, G.nodes[k][attribute_name]))
        Lter.append(k)
        
        del G.nodes[k][attribute_name]
    
    print("attributes removed")
    print("computing adjency matrix an adjency matrix normalized")
    
    node_order = G.nodes
    A = np.array(nx.adjacency_matrix(G, nodelist=node_order).todense())
    
    np.seterr(divide='ignore', invalid='ignore')
    
    S = np.zeros((np.shape(A)[0], np.shape(A)[1]))
    
    somme = np.sum(A,axis=1)
    
    for j in range (0, np.shape(A)[0]):
        for k in range (0, np.shape(A)[0]):
            S[j,k] = A[j,k]/somme[j]

    S[np.isnan(S)] = 0
   
    print("constructing initial vector of label")
    
    initial_vector = np.zeros((nbOfNodes,1))
    length = len(L)
    for l in range(0,nbOfNodes):
        
        test = 0
        for m in range(0, length):
            if(L[m][0] == l):
                test = 1
                
        if(test == 1):
            continue
        else:
            initial_vector[l,0] = G.nodes[l][attribute_name]
    
    result = copy.deepcopy(initial_vector)
    
    print("label propagation")
    
    x  = random.randint(0,length - 1)
    y = L[x][0]
    z = result[y][0]
    i = 0
    cond = True
    while (cond):
        i = i+1
        
        result = np.dot(S,result)
        
        for n in range(0,nbOfNodes):
            
            test = 0
            for o in range(0, length):
                if(L[o][0] == n):
                    test = 1
                
            if(test == 1):
                   continue
            else:
                result[n,0] = initial_vector[n,0]
        
       
        if(abs(z-result[y][0]) < 0.000000000000001):
            cond = False
        else:
            z = result[y][0]
          
    print("convergence en " + str(i) + " itérations")
    print("label choice")

    for q in range(0,length):
        a = result[L[q][0],0]
        
        for r in range(0,lengthbis - 1):
            if(a >= LBis[r] and a <= LBis[r+1]):
                distance = LBis[r+1] - LBis[r]
                
                if(a >= (LBis[r] +(distance/2))):
                    result[L[q][0],0] = LBis[r+1]
                else:
                    result[L[q][0],0] = LBis[r]
    
    print('computing label prediction accuracy')
    
    s = 0
    
    for t in range(0,length):
        if(result[L[t][0],0] == L[t][1]):
            s = s + 1
    
    return(round(s/length, 4))
 
        

print(propag_label_test(G1,0.4,'major_index'))
print(propag_label_test(G1,0.4,'dorm'))
print(propag_label_test(G1,0.4,'gender'))
print(propag_label_test(G2,0.4,'major_index'))
print(propag_label_test(G2,0.4,'dorm'))
print(propag_label_test(G2,0.4,'gender'))

print(propag_label_test(G3,0.4,'major_index'))
print(propag_label_test(G3,0.4,'dorm'))
print(propag_label_test(G3,0.4,'gender'))

print(propag_label_test(G4,0.4,'major_index'))
print(propag_label_test(G4,0.4,'dorm'))
print(propag_label_test(G4,0.4,'gender'))

print(propag_label_test(G5,0.4,'major_index'))
print(propag_label_test(G5,0.4,'dorm'))
print(propag_label_test(G5,0.4,'gender'))

print(propag_label_test(G6,0.4,'major_index'))
print(propag_label_test(G6,0.4,'dorm'))
print(propag_label_test(G6,0.4,'gender'))

print(propag_label_test(G7,0.4,'major_index'))
print(propag_label_test(G7,0.4,'dorm'))
print(propag_label_test(G7,0.4,'gender'))

print(propag_label_test(G8,0.4,'major_index'))
print(propag_label_test(G8,0.4,'dorm'))
print(propag_label_test(G8,0.4,'gender'))

print(propag_label_test(G9,0.4,'major_index'))
print(propag_label_test(G9,0.4,'dorm'))
print(propag_label_test(G9,0.4,'gender'))

print(propag_label_test(G10,0.4,'major_index'))
print(propag_label_test(G10,0.4,'dorm'))
print(propag_label_test(G10,0.4,'gender'))











