# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 14:55:23 2022

@author: Brauner Vladimir
"""

#initialisation 

import networkx as nx
import numpy as np
import scipy as sp
from numpy import linalg as LA
import pandas as pd
import csv
import matplotlib.pylab as plt

import networkx as nx
from packaging import version
import sys 

print("Python version:", sys.version)
print("networkx version:", nx.__version__)

# assert networkx version is greater or equal to 2.6
assert version.parse(nx.__version__) >= version.parse("2.6")

# assert python version is greater that 3.7
assert sys.version_info[0] == 3
assert sys.version_info[1] >= 7 

# If working in colab mount the drive filesystem 
if 'google.colab' in str(get_ipython()):
    print('Working in colab')
    
    from google.colab import drive
    drive.mount('/content/drive')
else:
    print("working locally")




# load graphs (networkx)

#GCaltech = nx.read_graphml("../fb100/fb100/Caltech36.graphml", node_type=int)
#GMit = nx.read_graphml("../fb100/fb100/MIT8.graphml", node_type=int)
GJohnsHopkins = nx.read_graphml("../fb100/fb100/Johns Hopkins55.graphml", node_type=int)


def plotDistributionDegree(G):
    
    numberOfNodes = G.number_of_nodes()
    degreeOfEachNodeList = []
    
    print('Nombre de noeuds du graph :')
    print(numberOfNodes)
    
    #construct the list of each node's degree
    for i in range (0,numberOfNodes):
        degreeOfEachNodeList.append(G.degree[i])
     
    #degree max of the graph
    maxDegree = max(degreeOfEachNodeList)
    

    abscisse = []
    ordonne  = []
    
    for j in range(0, maxDegree + 1):
        abscisse.append(j)
        count = 0
        
        for k in range(0, numberOfNodes):
            
            if (j == degreeOfEachNodeList[k]):
                count = count + 1
                
        #ordonne construction and normalisation
        ordonne.append(count/numberOfNodes)
    
    plt.figure()
    plt.xscale("log")
    plt.yscale("log")
    plt.plot(abscisse, ordonne, "+")
    
    plt.savefig("fig1.jpg", dpi=300)

plotDistributionDegree(GJohnsHopkins)













